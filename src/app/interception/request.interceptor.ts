import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpHeaders } from '@angular/common/http';
import { from, Observable } from 'rxjs';

import { AuthService } from '../auth/auth.service';
import { environment } from '../../environments/environment';

@Injectable()
export class RequestInterceptor implements HttpInterceptor {
  constructor(private authenticationService: AuthService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // add authorization header with basic auth credentials if available

    if (request.url.includes(environment.host + '/api/sensor/auth')) {
      return from(this.handleAccess(request, next));
    } else {
      return from(this.handleHasuraAccess(request, next));
    }
  }

  private async handleHasuraAccess(req: HttpRequest<any>, next: HttpHandler): Promise<HttpEvent<any>> {
    const token = await this.authenticationService.getToken();
    const userRoles = await this.authenticationService.getUserRole();
    const parseToken = JSON.parse(token);
    const parseRoles = JSON.parse(userRoles);

    const changedReq = req.clone({
      headers: new HttpHeaders({
        ...(parseToken !== null && parseToken.token !== '' && { Authorization: `Bearer ${parseToken.token}` }),
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true',
        'Access-Control-Allow-Methods': 'GET,HEAD,OPTIONS,POST,PUT',
        'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept, Authorization',
        ...(parseRoles &&
          parseRoles.roles.length > 0 &&
          parseRoles.roles.includes('user') && { 'X-Hasura-Role': 'user' }),
      }),
    });
    return next.handle(changedReq).toPromise();
  }

  private async handleAccess(req: HttpRequest<any>, next: HttpHandler): Promise<HttpEvent<any>> {
    const token = await this.authenticationService.getToken();
    const userRole = await this.authenticationService.getUserRole();
    const changedReq = req.clone({
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true',
        'Access-Control-Allow-Methods': 'GET,HEAD,OPTIONS,POST,PUT',
        'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept, Authorization',
      }),
    });

    return next.handle(changedReq).toPromise();
  }

  private async handleAccessSegnalazioni(req: HttpRequest<any>, next: HttpHandler): Promise<HttpEvent<any>> {
    const token = await this.authenticationService.getToken();
    const userRole = await this.authenticationService.getUserRole();
    const changedReq = req.clone({
      headers: new HttpHeaders({
        Authorization: `Basic ${btoa('admin:Krop0t!boO44')}`,
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true',
        'Access-Control-Allow-Methods': 'GET,HEAD,OPTIONS,POST,PUT',
        'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept, Authorization',
      }),
    });

    return next.handle(changedReq).toPromise();
  }
}
